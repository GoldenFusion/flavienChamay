### Hi, I'm Flavien! <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px">
[![Open To Work](https://img.shields.io/badge/OpenToWork-YES-red?style=flat&logo=gravatar&logoColor=white)]()
[![My CV Markdown](https://img.shields.io/badge/CV-In_MarkDown/Online-yellow?style=flat&logo=markdown&logoColor=white)](https://flavienchamay.github.io/digital-cv-flavien_chamay/)
[![My CV Latex](https://img.shields.io/badge/CV-In_Latex/PDF-green?style=flat&logo=latex&logoColor=white)](https://raw.githubusercontent.com/flavienChamay/digital-cv-flavien_chamay/gh-pages/my_nice_latex_resume.pdf)
[![GitHub Flavien](https://img.shields.io/github/followers/flavienChamay?label=follow&style=social)](https://github.com/flavienChamay)


## Tech and Math enthusiast.
- 🔭 I’m currently working at - Capgemini.
- 🌱 I’m currently learning - Python.
- ⚡ Fun fact - I like jogging and weight training and I'm interested in extraterrestrial life.

## :wrench: My Skills:
* [![Python](https://img.shields.io/badge/Language-Python_3-yellow?style=flat&logo=Python&logoColor=white)](https://github.com/flavienChamay?tab=repositories&q=&type=&language=python)
[![C](https://img.shields.io/badge/Language-C_17-yellow?style=flat&logo=C&logoColor=white)](https://github.com/flavienChamay?tab=repositories&q=&type=&language=c)
[![Latex](https://img.shields.io/badge/Language-Latex-yellow?style=flat&logo=latex&logoColor=white)](https://github.com/flavienChamay?tab=repositories&q=&type=&language=latex)
[![Bash](https://img.shields.io/badge/Language-Bash-yellow?style=flat&logo=bash&logoColor=white)](https://github.com/flavienChamay?tab=repositories&q=&type=&language=Bash)
* [![Django](https://img.shields.io/badge/Framework-Django_3.1-red?style=flat&logo=django&logoColor=white)](https://en.wikipedia.org/wiki/Django_(web_framework))
* [![Linux Manjaro](https://img.shields.io/badge/Linux_OS-Manjaro-blue?style=flat&logo=manjaro&logoColor=white)](https://en.wikipedia.org/wiki/Manjaro)
[![Linux RedHat](https://img.shields.io/badge/Linux_OS-Rhel-blue?style=flat&logo=redhat&logoColor=white)](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux)
* [![Git](https://img.shields.io/badge/VCS-Git-black?style=flat&logo=git&logoColor=white)](https://git-scm.com/)
* [![Emacs](https://img.shields.io/badge/IDE-Emacs-white?style=flat&logo=gnu-emacs&logoColor=white)](https://www.gnu.org/software/emacs/)
[![Vim](https://img.shields.io/badge/IDE-Vim-white?style=flat&logo=vim&logoColor=white)](https://www.vim.org/)

## :art: My Main Projects:
<a href="https://github.com/flavienChamay/Simple-Blockchain"><img height="100px" src="https://github-readme-stats.vercel.app/api/pin/?username=flavienChamay&repo=Simple-Blockchain&theme=algolia&show_icons=true" /> </a> <a href="https://github.com/flavienChamay/PyTacToe"> <img height="100px" src="https://github-readme-stats.vercel.app/api/pin/?username=flavienChamay&repo=PyTacToe&theme=algolia&show_icons=true" /> </a> <a href="https://github.com/flavienChamay/flavienchamay-personal-website"><img height="100px" src="https://github-readme-stats.vercel.app/api/pin/?username=flavienChamay&repo=flavienchamay-personal-website&theme=algolia&show_icons=true" /> </a>

## :chart_with_upwards_trend: My Stats on GitHub:
<a href="https://github.com/flavienChamay?tab=repositories"><img height="165px" src="https://github-readme-stats.vercel.app/api?username=flavienChamay&count_private=true&include_all_commits=true&theme=algolia&show_icons=true" /><!-- wi*quL3fcV --><img height="165px" src="https://github-readme-stats.vercel.app/api/top-langs/?username=flavienChamay&layout=compact&theme=algolia" /></a>

## :earth_africa: Connect with Me:
[<img align="left" alt="codeSTACKr | ProtonMail" width="22px" src="https://simpleicons.org/icons/protonmail.svg" />][protonmail]
[<img align="left" alt="codeSTACKr | StackOverFlow" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/stackoverflow.svg" />][stackoverflow]
<br />

<!-- Optional if you have blogs -->
<!-- ## Latest blog posts: -->
<!-- BLOG-POST-LIST:START -->
<!-- BLOG-POST-LIST:END -->

<!-- This section you create this variables that are used above -->
[protonmail]: mailto:flavien.chamay@protonmail.com
[stackoverflow]: https://stackoverflow.com/users/7347010/flavien-chamay?tab=profile
